package com.mycompany.testjdbcb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class will be our initial connection to a database.
 *
 * @since Sep 27, 2017
 * @author bjmaclean
 */
public class TestJdbcMain {

    public static void main(String[] args) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_ojt",
                    "cis2232_admin",
                    "Test1234");

            Statement stmt = conn.createStatement();
            try {
                stmt.executeUpdate("INSERT INTO user(userId, username, password, "
                        + "lastName, firstName, userTypeCode, additional1, additional2, "
                        + "createdDateTime) "
                        + "VALUES (4, 'wyatt', '202cb962ac59075b964b07152d234b70','Gallant',"
                        + " 'Wyatt',1,'','',now())");

            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
            }

            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from user");

            while (rs.next()) {
                System.out.println("First name: "+rs.getString(5));

                int numColumns = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= numColumns; i++) {
                    // Column numbers start at 1.
                    // Also there are many methods on the result set to return
                    //  the column as a particular type. Refer to the Sun documentation
                    //  for the list of valid conversions.
                    System.out.println("COLUMN " + i + " = " + rs.getObject(i));
                }
            }
        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            System.out.println(sqle);
        } finally {
            //It's important to close the connection when you are done with it
            try {
                conn.close();
            } catch (Throwable e) {
                System.out.println("Could not close JDBC Connection" + e);
            }
        }
    }
}
